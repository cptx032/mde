# coding: utf-8
# Migration data extraction
from __future__ import unicode_literals

from django.apps import AppConfig


class MdeConfig(AppConfig):
    name = 'mde'
