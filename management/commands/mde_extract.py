# coding: utf-8
from __future__ import print_function, unicode_literals

import glob
import importlib
import os
import re
from datetime import datetime
from io import BytesIO

from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import migrations
from six import text_type as unicode


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        self.obfuscation = {}
        self.initial_num_of_columns = {}
        self.rename_table_list = {}
        self.no_operations = []
        self.total_files = []
        return super(Command, self).__init__(*args, **kwargs)

    def get_obfuscation(self, key, value):
        if key not in self.obfuscation:
            self.obfuscation[key] = []
        if value not in self.obfuscation[key]:
            self.obfuscation[key].append(value)
        index = self.obfuscation[key].index(value)
        return index

    def fromisoformat(self, date_str):
        """Return a datetime.datetime instance from a ISO str."""
        return datetime(*[int(i) for i in date_str.split("T")[0].split("-")])

    def get_migration_files(self, folder):
        file_list = glob.glob(os.path.join(folder, "migrations/*.py"))
        file_list.sort()
        return file_list

    def get_release_migration_date(self, migration_file_path):
        """Return the date when the file was included in master branch.

        To this project this date is more important because we are
        interested in the date when the file in on master branch once
        that the file can have the description date manually changed.
        """
        migration_file_path = migration_file_path.replace("\\", "/")
        # we need to chdir the base folder cause some folders
        # maybe a gitsubmodule
        current_folder = os.getcwd()
        os.chdir(migration_file_path.split("/")[0])
        migration_file_path = "/".join(migration_file_path.split("/")[1:])
        command = "git log --format=%cI {}".format(migration_file_path)
        with os.popen(command) as _f:
            # the git will return many dates, here we are getting only the last
            # one
            dates = [
                self.fromisoformat(dt) for dt in _f.read().strip().split("\n")
            ]
            dates.sort()
            result = dates[0]
        os.chdir(current_folder)
        return result

    def fake_sql_from_operations(self, operations, project_name):
        for operation in operations:
            t = type(operation)
            if t is migrations.AlterField:
                continue
            elif t is migrations.AlterModelOptions:
                continue
            elif t is migrations.RunPython:
                continue
            elif t is migrations.RenameField:
                yield 'ALTER TABLE "{}_{}" RENAME COLUMN TO'.format(
                    project_name.strip().lower(),
                    operation.model_name.strip().lower()
                )
            elif t is migrations.RemoveField:
                yield 'ALTER TABLE "{}_{}" DROP COLUMN'.format(
                    project_name.strip().lower(),
                    operation.model_name.strip().lower()
                )
            elif t is migrations.AddField:
                yield 'ALTER TABLE "{}_{}" ADD COLUMN "{}"'.format(
                    project_name.strip().lower(), operation.model_name.strip().lower(), operation.name
                )
            elif t is migrations.CreateModel:
                yield 'CREATE TABLE "{}_{}" ({})'.format(
                    project_name.strip().lower(),
                    operation.name.strip().lower(),
                    ", ".join([field[0] for field in operation.fields])
                )
            elif t is migrations.DeleteModel:
                yield 'DROP TABLE "{}_{}"'.format(
                    project_name.strip().lower(), operation.name.strip().lower()
                )
            elif t is migrations.AlterUniqueTogether:
                yield 'ALTER TABLE "{}_{}" ADD CONSTRAINT UNIQUE'.format(
                    project_name.strip().lower(),
                    operation.name.strip().lower()
                )
            elif t is migrations.RenameModel:
                yield 'ALTER TABLE "{}_{}" RENAME TO "{}_{}"'.format(
                    project_name.strip().lower(),
                    operation.old_name.strip().lower(),
                    project_name.strip().lower(),
                    operation.new_name_lower,
                )
            else:
                import ipdb; ipdb.set_trace(context=10)

    def get_sql_commands_in_migration(self, file_name):
        project_name, _, migration_file_name = file_name.split(".")
        try:
            output_lines = call_command(
                "sqlmigrate",
                project_name,
                migration_file_name,
                stdout=BytesIO(),
            ).split(";")
            temp_list = []
            for i in [j.strip() for j in output_lines if j.strip()]:
                line = []
                for k in i.split("\n"):
                    if not k.strip().startswith("--"):
                        line.append(k.strip())
                temp_list.append("\n".join(line))
            output_lines = temp_list
        except ValueError:
            lib = importlib.import_module(file_name)
            if getattr(lib, "Migration", None):
                for i in self.fake_sql_from_operations(
                    lib.Migration.operations, project_name
                ):
                    yield i
            else:
                print("ERROR GETTING SQL: {}".format(migration_file_name))
            return

        for sql_command in output_lines:
            if sql_command.startswith("--"):
                continue
            elif sql_command.startswith("BEGIN"):
                continue
            elif sql_command.startswith("COMMIT"):
                continue
            elif not sql_command.strip():
                continue
            # data manipulation will not enter
            elif sql_command.startswith("UPDATE"):
                continue
            elif sql_command.startswith("CREATE EXTENSION"):
                continue
            elif sql_command.startswith("SET CONSTRAINTS"):
                continue
            yield sql_command

    def have_this_words(self, words, sql):
        for i in words:
            if i not in sql:
                return False
        return True

    def add_foreign_key(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ADD CONSTRAINT", "FOREIGN KEY"], sql
        )

    def drop_foreign_key(self, sql):
        is_drop_constraint = self.have_this_words(
            ["ALTER TABLE", "DROP CONSTRAINT"], sql
        )
        if not is_drop_constraint:
            return False
        c_name = (
            re.findall(r'DROP CONSTRAINT "\w+"', sql)[0]
            .split()[-1]
            .replace('"', "")
        )
        return "_fk_" in c_name

    def create_table(self, sql):
        # many operations are inside a single CREATE TABLE command
        # like creating primary keys, but here we are considering as a single
        # operation in terms of database
        return self.have_this_words(["CREATE TABLE"], sql)

    def get_number_of_columns(self, sql):
        founds = re.findall(r'\(.+\)', sql)
        return str(len(founds[0].split(",")))

    def get_table_name(self, sql):
        table_name = re.findall(r'CREATE TABLE \"\w+\"', sql)[0].split('CREATE TABLE')[-1].replace('"', '').strip()
        return self.get_first_table_name(table_name)

    def add_key(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ADD CONSTRAINT", "UNIQUE"], sql
        )

    def add_column(self, sql):
        return self.have_this_words(["ALTER TABLE", "ADD COLUMN"], sql)

    def get_column_name(self, sql):
        return re.findall(r'ADD COLUMN "\w+\"', sql)[0]

    def add_index(self, sql):
        return self.have_this_words(["CREATE INDEX", "ON"], sql)

    def drop_default(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ALTER COLUMN", "DROP DEFAULT"], sql
        )

    def rename_column(self, sql):
        return self.have_this_words(["ALTER TABLE", "RENAME COLUMN", "TO"], sql)

    def add_default(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ALTER COLUMN", "SET DEFAULT"], sql
        )

    def change_column_type(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ALTER COLUMN", "TYPE", "USING"], sql
        )

    def drop_column(self, sql):
        return self.have_this_words(["ALTER TABLE", "DROP COLUMN"], sql)

    def drop_table(self, sql):
        return self.have_this_words(["DROP TABLE"], sql)

    def drop_not_null(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ALTER COLUMN", "DROP NOT NULL"], sql
        )

    def add_constraint(self, sql):
        return self.have_this_words(
            ["ALTER TABLE", "ADD CONSTRAINT", "CHECK"], sql
        )

    def rename_table(self, sql):
        return self.have_this_words(["ALTER TABLE", "RENAME TO"], sql)

    def get_alter_table_name(self, sql):
        table_name = re.findall(r'ALTER TABLE \"\w+\"', sql)[0].split('ALTER TABLE')[-1].replace('"', '').strip()
        return self.get_first_table_name(table_name)

    def get_add_index_table_name(self, sql):
        table_name = re.findall(r'ON \"\w+\"', sql)[0].split("ON")[-1].replace('"', "").strip()
        return self.get_first_table_name(table_name)

    def get_drop_table_name(self, sql):
        table_name = re.findall(r'DROP TABLE \"\w+\"', sql)[0].split("DROP TABLE")[-1].replace('"', "").strip()
        return self.get_first_table_name(table_name)

    def get_first_table_name(self, table_name):
        """Return the first table name considering that the table was renamed.
        """
        if table_name not in self.rename_table_list:
            return table_name.strip().lower()
        max_iterations = 10
        iterations = 0
        while True:
            iterations += 1
            if iterations > max_iterations:
                import ipdb; ipdb.set_trace(context=10)
                break
            table_name = self.rename_table_list[table_name]
            if table_name not in self.rename_table_list:
                return table_name.strip().lower()

    def get_qiu_code_from_sql(self, sql):
        if self.create_table(sql):
            table_name = self.get_table_name(sql)
            obfuscated_name = "table_{}".format(self.get_obfuscation("table_name", table_name))
            self.initial_num_of_columns[obfuscated_name] = self.get_number_of_columns(sql)
            return "A1", table_name
        elif self.add_column(sql):
            return "A2", self.get_alter_table_name(sql)
        elif self.drop_table(sql):
            return "A4", self.get_drop_table_name(sql)
        elif self.rename_table(sql):
            old_table_name = re.findall(r'ALTER TABLE "\w+"', sql)[0].replace('ALTER TABLE', "").replace('"', '').strip()
            new_table_name = re.findall(r'RENAME TO "\w+"', sql)[0].replace('RENAME TO', '').replace('"', '').strip()
            self.rename_table_list[new_table_name.strip().lower()] = old_table_name.strip().lower()
            return "A5", self.get_alter_table_name(sql)
        elif self.drop_column(sql):
            return "A6", self.get_alter_table_name(sql)
        elif self.rename_column(sql):
            return "A7", self.get_alter_table_name(sql)
        elif self.change_column_type(sql):
            return "A8", self.get_alter_table_name(sql)
        elif self.add_key(sql):
            return "A10", self.get_alter_table_name(sql)
        elif self.add_foreign_key(sql):
            return "A12", self.get_alter_table_name(sql)
        elif self.drop_foreign_key(sql):
            return "A13", self.get_alter_table_name(sql)
        elif self.add_constraint(sql):
            return "A16", self.get_alter_table_name(sql)
        elif self.add_index(sql):
            return "A18", self.get_add_index_table_name(sql)
        elif self.add_default(sql):
            return "A20", self.get_alter_table_name(sql)
        elif self.drop_default(sql):
            return "A21", self.get_alter_table_name(sql)
        elif self.drop_not_null(sql):
            return "A24", self.get_alter_table_name(sql)

        # import ipdb; ipdb.set_trace(context=10)
        return "undefined", "undefined"

    def get_migration_infos(self, migration_file_path):
        import_file_path = (
            migration_file_path.replace("/", ".")
            .replace("\\", ".")
            .replace(".py", "")
        )
        lib = importlib.import_module(import_file_path)
        if not getattr(lib, "Migration", None):
            return

        general_keys = {
            "project_name": import_file_path.split(".")[0],
            "file_name": import_file_path,
            "release_date": self.get_release_migration_date(
                migration_file_path
            ),
        }

        qiu_codes = ["A" + str(i + 1) for i in range(0, 26)]

        operations = list(self.get_sql_commands_in_migration(import_file_path))
        if not operations:
            self.no_operations.append(import_file_path)
        self.total_files.append(import_file_path)
        operations_counter = {}
        for sql_operation in operations:
            qiu_code, table_name = self.get_qiu_code_from_sql(sql_operation)
            if qiu_code == "undefined":
                continue
            if table_name not in operations_counter:
                operations_counter[table_name] = {k: 0 for k in qiu_codes}
            operations_counter[table_name][qiu_code] += 1
        for table_name in operations_counter:
            list_to_show = [
                "project_{}".format(self.get_obfuscation("project_name", general_keys["project_name"])),
                "file_{}".format(self.get_obfuscation("file_name", general_keys["file_name"])),
                general_keys["release_date"].strftime("%Y-%m-%d"),
                "table_{}".format(self.get_obfuscation("table_name", table_name)),
            ]
            for i in qiu_codes:
                list_to_show.append(operations_counter[table_name][i])
            table_obfuscated = "table_{}".format(
                self.get_obfuscation("table_name", table_name)
            )
            number_of_columns = self.initial_num_of_columns.get(table_obfuscated, 0)
            if number_of_columns == 0:
                import ipdb; ipdb.set_trace(context=10)
            list_to_show.append(number_of_columns)
            yield list_to_show

    def handle(self, *args, **kwargs):
        # fixme > add an argument to set the destiny folder
        # fixme > add an argument to set a exclusion list
        # fixme > add an argument to set the folder list
        folders = [
            folder
            for folder in glob.glob("*")
            if os.path.isdir(os.path.join(folder, "migrations"))
        ]
        folders.sort()
        qiu_codes = ["A" + str(i + 1) for i in range(0, 26)]
        print("Project;File;Release Date;Table Name;" + ";".join(qiu_codes) + ";Number Of Columns")
        for folder in folders:
            migration_files = self.get_migration_files(folder)
            for migration_file in migration_files:
                for info in self.get_migration_infos(migration_file):
                    print(";".join([str(i) for i in info]))
        print("TOTAL FILES", len(self.total_files))
        print("NO OP", len(self.no_operations))
        print(self.obfuscation)
        print(self.initial_num_of_columns)

