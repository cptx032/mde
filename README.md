# MDE EXTRACT

Script used to analize the migration files from Django Projects using category codes described in Qiu, Dong; Li, Bixin (Southeast University, China); Su, Zhendong (University of California, Davis, U. (2013). An Empirical Analysis of the Co-evolution of Schema andCode in Database Applications. ESEC/FSE, 18–26. https://www.cs.ucdavis.edu/~su/publications/fse13-db-study.pdf

### Usage
- install the mde application as a folder in the root path of project
- include the `mde` in the INSTALLED_APPS in settings.py
- run: `./manage.py mde_extract > report.txt`

### Output
The output is a CSV file format with additional informations in the end

## Data Analised
https://gitlab.com/cptx032/pub-csv-atomic-operations